//creation des variables globales pour faire tourner le bouzin
let premierCoup = true
let premiereCarte;
let deuxiemeCarte;
let pairetrouvee = 0;
let score = 0;
let cptScore = document.getElementById("cptScore");
let cptReste = document.getElementById("cptReste");
let nombreDeCartes = sessionStorage.getItem("taille");
let chemin = "";
let extension = "";
if (nombreDeCartes == null){nombreDeCartes = 12;}; // si aucun utilisateur n'est enregistré, le plateau comportera 12 cartes
if(!localStorage.getItem("HiScores")){localStorage.setItem("HiScores", "[]")}; // si aucun HiScore n'est enregistré, le tableau sera vide
if(!sessionStorage.getItem("user")){sessionStorage.setItem("user", "anonyme")}; //si aucun utilisateur n'est enregistré, un utilisateur Anonyme servira aux divers enregistrements 

window.onload = init;

// cette ligne configure la touche espace pour qu'elle reinitialise la page
document.addEventListener("keydown", (e) => {if (e.key==" "){window.location.reload()}} );

function init(){
tableauHiScores();
choixDuJeu();
let jeuDeCarte =[];

// on remplit le tableau avec les cartes, qui sont créées avec un div, une image et un eventListener. Chaque image est attribuée en double.
for (i=0;i<nombreDeCartes;i++){
    let nouvelleCarte = creationDeCarte(i);
    jeuDeCarte.push(nouvelleCarte);
}

jeuDeCarte = melange(jeuDeCarte);
installationTapis();
distribution(jeuDeCarte);

// premiere mise a jour des compteurs pour le debut de partie
majCompteurs();
cptScore.innerText = score;
cptReste.innerText = (nombreDeCartes/2)-pairetrouvee;
}

//cette fonction prepare les images selon le choix de jeu du joueur
function choixDuJeu(){
    let jeuChoisi = sessionStorage.getItem("tableau");
    if (!jeuChoisi){jeuChoisi == "dinosaures"};
    chemin = "Enonce/ressources/"+jeuChoisi+"/";
    if (jeuChoisi == "alphabet-scrabble"){extension = ".png"};
    if (jeuChoisi == "animaux" || jeuChoisi == "animauxAnimes" || jeuChoisi == "chiens"){extension = ".webp"};
    if (jeuChoisi == "animauxdomestiques" || jeuChoisi=="dinosaures" || jeuChoisi=="dinosauresAvecNom"){extension = ".jpg"};
    if (jeuChoisi == "memory-legume"){extension = ".svg"};  
}

// cette fonction prepare la zone de jeu en ajustant le nombre de colonnes du tableau par rapport aux nombres de cartes jouées
function installationTapis(){
    tapisDeJeu = document.getElementById("tapisDeJeu");
    let nbdecolonnes;
    if (nombreDeCartes <= 20){nbdecolonnes = 4;}
    if (nombreDeCartes <= 30 && nombreDeCartes>20){nbdecolonnes = 6;}
    if (nombreDeCartes <= 40 && nombreDeCartes>30){nbdecolonnes = 8;}
    if (nombreDeCartes>40){nbdecolonnes = 10;}
    tapisDeJeu.style.gridTemplateColumns = "repeat("+nbdecolonnes+", 1fr)";
}

// cette fonction crée chaque carte individuellement, en mettant deux fois la même image à la suite. 
//la carte contient un div, une img dans le div, et un EventListener pour être cliquable
// l'image de la carte est determinée par la fonction choix du jeu
function creationDeCarte (i){
    let carteVerso = document.createElement("div");
    carteVerso.setAttribute("class", "carteVerso");
    carteVerso.setAttribute("id", "carteVerso"+i)
  
    let carteRecto = document.createElement("img");
    carteRecto.setAttribute("class", "carteRecto");
    carteRecto.setAttribute("id", "carteRecto"+i);
    let valeurDeImage = Math.trunc(1+(i/2));
    carteVerso.dataset.valeur = valeurDeImage;
    carteRecto.setAttribute("src", chemin+valeurDeImage+extension)
    carteVerso.appendChild(carteRecto);
    carteVerso.addEventListener("click", cliquageCarte);
    return carteVerso;
}

 // cette fonction recoit le tableau qui contient les cartes dans l'ordre, et les mélange de facon aléatoire
function melange (tableau1){ 
   
let longueurTotal = tableau1.length;
let longueur = 0;
let tableauMelange = []
let indice = 0;
for (i=0;i<longueurTotal;i++) {
longueur = tableau1.length; 
indice = Math.trunc(Math.random()*longueur);  
tableauMelange[i] = tableau1[indice];  
tableau1.splice(indice, 1);
}
return tableauMelange;
}

//cette fonction crée le tapis de jeu en distribuant les cartes dans l'ordre du tableau (donc aléatoire grace a la fonction precedente)
function distribution (jeuDeCarte){
    for (i=0;i<jeuDeCarte.length;i++){
        tapisDeJeu = document.getElementById("tapisDeJeu");
        tapisDeJeu.append(jeuDeCarte[i]);
    }
}

// cette fonction gere le cliquage de carte, et determine si c'est la premiere ou la deuxieme carte retournee. 
//Si c'est la premiere elle affiche la carte, et retire sa "cliquabilite", et si c'est la deuxieme elle effectue la comparaison
function cliquageCarte(event){
    if (premiereCarte && deuxiemeCarte) {
        return;
    }

    let carteCliquee = event.currentTarget;
    if (premierCoup){
        retournement(carteCliquee); 
        premiereCarte = carteCliquee;
        premiereCarte.removeEventListener("click", cliquageCarte);
        premierCoup = false;
    } else {
        retournement(carteCliquee);
        deuxiemeCarte = carteCliquee;
        deuxiemeCarte.removeEventListener("click", cliquageCarte);
        score++;
        comparaison();
        premierCoup = true;
        majCompteurs();
    }
}

//cette fonction gere l'animation de retournement des cartes, de Verso a recto
function retournement(carte){
    carte.style.transform="scale(0, 1)";
    setTimeout( () => {carte.firstChild.style.display="block"}, 200);
    setTimeout( () => {carte.style.transform="scale(1, 1)"}, 200);
    //carte.firstChild.style.display="block";
    
    
}

// cette fonction compare les deux cartes retournées. si les deux cartes sont identiques, elle enleve la "cliquabilite"
// elle declenche egalement la victoire le cas échéant
function comparaison(){
    if (premiereCarte.dataset.valeur != deuxiemeCarte.dataset.valeur){
    setTimeout (cacherCarte, 1000, premiereCarte, deuxiemeCarte);
    } else {
    premiereCarte.removeEventListener("click", cliquageCarte);
    deuxiemeCarte.removeEventListener("click", cliquageCarte);
    pairetrouvee++;
    majCompteurs();
    premiereCarte = null;
    deuxiemeCarte = null;
    if(pairetrouvee == (nombreDeCartes/2)){
       victoire();
    }
    }
}

// cette fonction re-cache les deux cartes, avec animation, si elles ne sont pas identiques, et réactive leur "cliquabilité" pour la suite du jeu
function cacherCarte(carte1, carte2){
    premiereCarte.addEventListener("click", cliquageCarte);
    deuxiemeCarte.addEventListener("click", cliquageCarte);
    carte1.style.transform="scale(0, 1)";
    setTimeout( () => {carte1.firstChild.style.display="none"}, 200);
    setTimeout( () => {carte1.style.transform="scale(1, 1)"}, 200);
    carte2.style.transform="scale(0, 1)";
    setTimeout( () => {carte2.firstChild.style.display="none"}, 200);
    setTimeout( () => {carte2.style.transform="scale(1, 1)"}, 200);
    premiereCarte = null;
    deuxiemeCarte = null;

}

// cette fonction gere les compteurs affichés à l'ecran
function majCompteurs(){
    cptScore.innerText = score;
    cptReste.innerText = (nombreDeCartes/2)-pairetrouvee;
}

// cette fonction modifie la zone des compteurs pour afficher le score
function victoire() {
    document.getElementById("interfaceDeJeu").style.transform = "rotate(1440deg) scale(2,2)";
    setTimeout (() => {document.getElementById("interfaceDeJeu").style.transform = "scale(1,1)"}, 800);
    document.getElementById("tapisDeJeu").remove();
    document.getElementById("affichagePaires").remove();
    document.getElementById("affichageScore").remove();
    messageVictoire = document.createElement("li");
    messageVictoire.innerHTML = "Bravo ! Vous avez réussi cette <br> partie en <strong>"+score+"</strong> coups !";
    document.getElementById("listeCompteurs").style.textAlign =("center");
    document.getElementById("listeCompteurs").style.fontSize =("3rem");
    document.getElementById("listeCompteurs").prepend(messageVictoire);
    enregistrementPartie();
    tableauHiScores();
    
}

// cette fonction enregistre la partie dans l'historique du joueur, dans le session storage
function enregistrementPartie(){
    let tableauDeParties = JSON.parse(sessionStorage.getItem("historique"));
    let partie = { 
        "Score" : score,
        "Jeu" : sessionStorage.getItem("tableau"),
        "CartesATrouver" : nombreDeCartes,
    }
    tableauDeParties.unshift(partie);
    tableauDeParties = tableauDeParties.slice(0,10);
    sessionStorage.setItem("historique", JSON.stringify(tableauDeParties));

    sauvegardehistorique();
    sauvegardeHiScore();
}

// cette fonction sauvegarde l'historique depuis le session storage dans le local storage
function sauvegardehistorique() {
    let memoireLocale = JSON.parse(localStorage.getItem("utilisateursStorage"));
    memoireLocale.forEach(element => {
        if (element.name == sessionStorage.getItem("user")){
            element.historique = JSON.parse(sessionStorage.getItem("historique"));
        }
    localStorage.setItem("utilisateursStorage", JSON.stringify(memoireLocale));
    });
}

// cette fonction sauvegarde les meilleurs scores dans le Local Storage
function sauvegardeHiScore(){
    let hiscores = JSON.parse(localStorage.getItem("HiScores"));
    let date = new Date().toLocaleDateString("fr");
    let resumepartie = {
        "joueur" : sessionStorage.getItem("user"),
        "score" : score,
        "nombredecartes" : nombreDeCartes,
        "date": date,
    }
hiscores.push(resumepartie);
hiscores.sort((a, b) => {
    if (a.score < b.score)
       return -1;
    if (a.score > b.score )
       return 1;
    return 0;
  });

  hiscores = hiscores.slice(0,5);
  localStorage.setItem("HiScores", JSON.stringify(hiscores));
  console.log(hiscores);
}

// cette fonction sert a l'affichage du tableau des meilleurs scores
function tableauHiScores(){
    let historique = JSON.parse(localStorage.getItem("HiScores"));
    document.getElementById("tableauHiScores").innerHTML="<tr><th>Joueur</th><th>Score</th><th>Nombre de cartes</th><th>Date</th></tr>";
    historique.forEach(element => {
        let ligne = document.createElement("tr");
        let case1 = document.createElement("td");
        case1.innerText = (element.joueur);
        ligne.append(case1);
        let case2 = document.createElement("td");
        case2.innerText = (element.score);
        ligne.append(case2);
        let case3 = document.createElement("td");
        case3.innerText = (element.nombredecartes);
        ligne.append(case3);
        let case4 = document.createElement("td");
        case4.innerText = (element.date);
        ligne.append(case4);
        document.getElementById("tableauHiScores").append(ligne);
    });
}