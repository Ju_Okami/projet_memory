menuSet();

function menuSet() {
    menu = document.getElementById("barrenav")
    if(sessionStorage.getItem("user") == null || sessionStorage.getItem("user") == "anonyme"){
        menu_Anonyme(menu);
    } else {
        menu_Connecte(menu);
    }
}

function menu_Anonyme (menu){
    menu.innerHTML = '<a href=index.html target="_self"><li>Accueil</li></a>'+
    '<a href=inscription.html target="_self"><li>Inscription</li></a>'+
    '<a href=login.html target="_self"><li>Se connecter</li></a>'+
    '<a href=jeu.html target="_self"><li>Jouer</li></a>';
}

function menu_Connecte (menu){
    menu.innerHTML = '<a href=index.html target="_self"><li>Accueil</li></a>'+
    '<a href=profil.html target="_self"><li>Profil</li></a>'+
    '<a href=jeu.html target="_self"><li>Jouer</li></a>'+
    '<a href=logout.html target="_self"><li>Déconnexion</li></a>';
}