let tableauchoisi = sessionStorage.getItem("tableau");


window.onload = init;

function init() {

    document.getElementById(sessionStorage.getItem("tableau")).setAttribute("selected", "");
    if(sessionStorage.getItem("user") == null){
        pasdeprofil();
    } else {
        affichageprofil();
    }
    document.getElementById("listetableau").addEventListener("click", choixTableau);
    document.getElementById("listetaille").addEventListener("click", choixTaille);
    sauvegardeProfil();

}

function pasdeprofil(){
    window.location.href="login.html";
    return;
}

function affichageprofil(){

    document.getElementById("nomJoueur").innerText=sessionStorage.getItem("user");
    document.getElementById("mailJoueur").innerText=sessionStorage.getItem("mail");
    tableauParties();
    choixTableau();
    creerlistetaille(tableauchoisi);
    choixTaille();
}

function tableauParties(){
    let historique = JSON.parse(sessionStorage.getItem("historique"));
    historique.forEach(element => {
        let ligne = document.createElement("tr");
        let case1 = document.createElement("td");
        case1.innerText = (element.Score);
        ligne.append(case1);
        let case2 = document.createElement("td");
        case2.innerText = (element.Jeu);
        ligne.append(case2);
        let case3 = document.createElement("td");
        case3.innerText = (element.CartesATrouver);
        ligne.append(case3);
        document.getElementById("tableauParties").append(ligne);

    });
}

function choixTableau(){
    let selecteur = document.getElementById("listetableau");
    if (!selecteur.options[selecteur.selectedIndex].value){return;}
    tableauchoisi = selecteur.options[selecteur.selectedIndex].value;
    affichageTableau(tableauchoisi);
    sessionStorage.setItem("tableau", tableauchoisi);
    creerlistetaille(tableauchoisi);
    sauvegardeProfil();
}

function affichageTableau(tableau){
    image = document.getElementById("tableauchoisi");
    switch (tableau){
        case "alphabet-scrabble" : image.setAttribute("src", "Enonce/ressources/alphabet-scrabble/memory_detail_scrabble.png");break;
        case "animaux" : image.setAttribute("src", "Enonce/ressources/animaux/memory_detail_animaux.png");break;
        case "animauxAnimes" : image.setAttribute("src", "Enonce/ressources/animauxAnimes/memory_detail_animaux_animes.png");break;
        case "animauxdomestiques" : image.setAttribute("src", "Enonce/ressources/animauxdomestiques/memory_detail_animaux_domestiques.png");break;
        case "chiens" : image.setAttribute("src", "Enonce/ressources/chiens/memory_details_chiens.png");break;
        case "dinosaures" : image.setAttribute("src", "Enonce/ressources/dinosaures/memory_detail_dinosaures.png");break;
        case "dinosauresAvecNom" : image.setAttribute("src", "Enonce/ressources/dinosauresAvecNom/memory_details_dinosaures_avec_nom.png");break;
        case "memory-legume" : image.setAttribute("src", "Enonce/ressources/memory-legume/memory_detail.png");break;
    }
}

function creerlistetaille(tableau){
    selecteur = document.getElementById("listetaille");
    selecteur.innerHTML="";

    selecteur.append(creerelementlistetaille(4, "4 par 1"));
    selecteur.append(creerelementlistetaille(8, "4 par 2"));
    selecteur.append(creerelementlistetaille(12, "4 par 3"));
    if (tableau == "memory-legume"){return;}
    selecteur.append(creerelementlistetaille(16, "4 par 4"));
    if (tableau == "animauxAnimes"){return;}
    selecteur.append(creerelementlistetaille(20, "5 par 4"));
    if (tableau == "animauxdomestiques" || tableau == "dinosaures" || tableau == "dinosauresAvecNom"){return;}
    selecteur.append(creerelementlistetaille(24, "6 par 4"));
    selecteur.append(creerelementlistetaille(30, "6 par 5"));
    selecteur.append(creerelementlistetaille(32, "8 par 4"));
    selecteur.append(creerelementlistetaille(40, "8 par 5"));
    if (tableau == "chiens"){return;}
    selecteur.append(creerelementlistetaille(50, "10 par 5"));
}

function creerelementlistetaille(valeur, intitule) {
    option = document.createElement("option");
    option.setAttribute("value", valeur);
    option.setAttribute("id", valeur);
    if (valeur == sessionStorage.getItem("taille")){option.setAttribute("selected", "");};
    option.innerText=intitule;
    return option;
}

function choixTaille () {
    let selecteur = document.getElementById("listetaille");
    taillechoisie = selecteur.options[selecteur.selectedIndex].value;
    sessionStorage.setItem("taille", taillechoisie);
    sauvegardeProfil();
}

function sauvegardeProfil(){
    let memoireLocale = JSON.parse(localStorage.getItem("utilisateursStorage"));
    memoireLocale.forEach(element => {
        if (element.name == sessionStorage.getItem("user")){
            element.tableau = sessionStorage.getItem("tableau");
            element.taille = sessionStorage.getItem("taille");
        }
    localStorage.setItem("utilisateursStorage", JSON.stringify(memoireLocale));
    });

}