window.onload = init;
let checkUserName = false;
let checkMail = false;
let checkMdp = false;
let checkConfirm = false;
let users = [];

function init(){

    document.getElementById("nom").addEventListener( "input", validation_User_Name);
    document.getElementById("mail").addEventListener("input", validation_Adresse_Mail);
    document.getElementById("mdp").addEventListener("input", validation_Mot_De_Passe);
    document.getElementById("mdp2").addEventListener("input", validation_Confirm);
    document.getElementById("bouton_Valider").addEventListener("click", envoiFormulaire);

    
}

function allumage_Bouton (){
    if (checkConfirm && checkMail && checkMdp && checkUserName){
        document.getElementById("bouton_Valider").disabled = false;
    } else {
        document.getElementById("bouton_Valider").disabled = true;
    }
}

function validation_User_Name(event){
    let username = event.currentTarget.value;
if(username.length < 3){
    document.getElementById("indicUsername").innerText = "Votre identifiant doit contenir au moins 3 caractères";
    document.getElementById("logoUsername").src="Enonce/ressources/error.svg";
    document.getElementById("logoUsername").style.visibility="visible";
    checkUserName = false;
} else{
    document.getElementById("indicUsername").innerText = "";
    document.getElementById("logoUsername").src="Enonce/ressources/check.svg";
    document.getElementById("logoUsername").style.visibility="visible";
    checkUserName = true;
};
if (username.length == 0){
    document.getElementById("indicUsername").innerText = ""; 
    document.getElementById("logoUsername").style.visibility="hidden";
    checkUserName = false;
}
allumage_Bouton();

}

function validation_Adresse_Mail(event){
    let adresseMail = event.currentTarget.value;
    let regex = new RegExp ("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
    if(!regex.test(adresseMail)){
        document.getElementById("indicMail").innerText = "Votre adresse mail doit être valide";
        document.getElementById("logoMail").src="Enonce/ressources/error.svg";
        document.getElementById("logoMail").style.visibility="visible";
        checkMail = false;
    } else{
        document.getElementById("indicMail").innerText = "";
        document.getElementById("logoMail").src="Enonce/ressources/check.svg";
        document.getElementById("logoMail").style.visibility="visible";
        checkMail = true;
    };
    if (adresseMail.length == 0){
        document.getElementById("indicMail").innerText = ""; 
        document.getElementById("logoMail").style.visibility="hidden";
        checkMail = false;
    }  
    allumage_Bouton();
}

function validation_Mot_De_Passe(event){
    let regex_au_moins_un_chiffre = new RegExp ("\\d");
    let regex_au_moins_un_symbole = new RegExp ("[!#$%&()*+\\-./:;<=>?@[\\]^_`{|}~]");
    let regex_au_moins_une_lettre = new RegExp ("\\w");
    let motDePasse = event.currentTarget.value;
    let typeCarac = 0
    if (regex_au_moins_un_chiffre.test(motDePasse)){typeCarac++};
    if (regex_au_moins_un_symbole.test(motDePasse)){typeCarac++};
    if (regex_au_moins_une_lettre.test(motDePasse)){typeCarac++};
   
    if (motDePasse.length == 0){
        force_du_mdp("nul");
        logo_mdp("nul");
        checkMdp = false;
    }
    if (motDePasse.length <6 && motDePasse.length > 0){
        force_du_mdp("faible");
        logo_mdp("rouge");
        checkMdp = false;
    }
    if (motDePasse.length >= 6 && typeCarac<2 ){
        force_du_mdp("faible");
        logo_mdp("rouge");
        checkMdp = false;
    }
    if (motDePasse.length>=6 && typeCarac>=2){
        force_du_mdp("moyen");
        logo_mdp("rouge");
        checkMdp = false;
    }
    if (motDePasse.length >=6 && typeCarac>=3){
        force_du_mdp("moyen");
        logo_mdp("vert");
        checkMdp = true;
    }
    if (motDePasse.length>=9 && typeCarac>=3){
        force_du_mdp("fort");
        logo_mdp("vert");
        checkMdp = true;
    }
   allumage_Bouton();
}

function force_du_mdp (valeur){
    switch (valeur){
        case "nul" : 
        document.getElementById("faible").style.visibility = "hidden";
        document.getElementById("moyen").style.visibility = "hidden";
        document.getElementById("fort").style.visibility = "hidden";
        break;
        case "faible" : 
        document.getElementById("faible").style.visibility = "visible";
        document.getElementById("moyen").style.visibility = "hidden";
        document.getElementById("fort").style.visibility = "hidden";
        break;
        case "moyen" : 
        document.getElementById("faible").style.visibility = "visible";
        document.getElementById("moyen").style.visibility = "visible";
        document.getElementById("fort").style.visibility = "hidden";
        break;
        case "fort" : 
        document.getElementById("faible").style.visibility = "visible";
        document.getElementById("moyen").style.visibility = "visible";
        document.getElementById("fort").style.visibility = "visible";
        break;
    }
}

function logo_mdp(valeur){
    switch (valeur){
        case "vert" :
        document.getElementById("logoPassword").src="Enonce/ressources/check.svg";
        document.getElementById("logoPassword").style.visibility="visible";
        break;
        case "rouge" : 
        document.getElementById("logoPassword").src="Enonce/ressources/error.svg";
        document.getElementById("logoPassword").style.visibility="visible";
        break;
        case "nul" : 
        document.getElementById("logoPassword").style.visibility="hidden";
        break;
    }
}

function validation_Confirm(event){
    let confirm = event.currentTarget.value;
    let Mdp = document.getElementById("mdp").value
    if (confirm.length>0 && !checkMdp){
        document.getElementById("logoConfirmPassword").src="Enonce/ressources/error.svg";
        document.getElementById("logoConfirmPassword").style.visibility="visible";
        document.getElementById("indicMdpConfirm").innerText="Le mot de passe n'est pas valide";
        checkConfirm = false;
    }
    if (confirm.length>0 && checkMdp && confirm != Mdp){
        document.getElementById("logoConfirmPassword").src="Enonce/ressources/error.svg";
        document.getElementById("logoConfirmPassword").style.visibility="visible";
        document.getElementById("indicMdpConfirm").innerText="Les deux mots de passe ne sont pas identiques";
        checkConfirm = false;
    }
    if (confirm.length>0 && checkMdp && confirm == Mdp){
        document.getElementById("logoConfirmPassword").src="Enonce/ressources/check.svg";
        document.getElementById("logoConfirmPassword").style.visibility="visible";
        document.getElementById("indicMdpConfirm").innerText="";
        checkConfirm = true;
    }
    if (confirm.length == 0){
        document.getElementById("logoConfirmPassword").style.visibility="hidden";
        document.getElementById("indicMdpConfirm").innerText="";
        checkConfirm = false;
    }
    allumage_Bouton();
}

function envoiFormulaire(){

    if (!localStorage.getItem("utilisateursStorage"))
    {localStorage.setItem("utilisateursStorage", "[]")};

    let nom = document.getElementById("nom").value;
    let mail = document.getElementById("mail").value;
    let mdp = document.getElementById("mdp").value;
    let users = JSON.parse(localStorage.getItem("utilisateursStorage"));
   
for (i=0; i<users.length;i++){
if (users[i].name == nom){
    document.getElementById("indicUsername").innerText = "Ce nom d'utilisateur est déjà pris";
    return;
}
}
for (i=0; i<users.length;i++){
    if (users[i].mail == mail){
        document.getElementById("indicMail").innerText = "Cette adresse mail est déjà associée à un profil";
        return;
    }
    }

let user = {
    "name": nom,
    "pass": mdp, 
    "mail": mail,
    "historique": [],
    "tableau" : "dinosaures",
    "taille" : "20"
}

users.push(user);

localStorage.setItem("utilisateursStorage", JSON.stringify(users));

window.location.href="bienvenue.html";
}
