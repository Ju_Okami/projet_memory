window.onload = init;
let checkNom = false;
let checkMdp = false;

function init(){

    document.getElementById("nom").addEventListener("input", saisieNom);
    document.getElementById("mdp").addEventListener("input", saisieMdp);
    document.getElementById("bouton_Valider").addEventListener("click", identification);
}

function allumage_Bouton (){
    if (checkMdp && checkNom){
        document.getElementById("bouton_Valider").disabled = false;
    } else {
        document.getElementById("bouton_Valider").disabled = true;
    }
}

function saisieNom(event){
    let nom = event.currentTarget.value;
    if (nom.length ==0){
        document.getElementById("indicUsername").innerText = "";
        checkNom = false;
    }
    if (nom.length <3 && nom.length != 0){
        document.getElementById("indicUsername").innerText = "Votre identifiant est au moins long de 3 caractères";
        checkNom = false;
    }
    if (nom.length >= 3){
        document.getElementById("indicUsername").innerText = "";
        checkNom = true;
    }
allumage_Bouton();
}

function saisieMdp(event){
    let nom = event.currentTarget.value;
    if (nom.length ==0){
        document.getElementById("indicMdp").innerText = "";
        checkMdp = false;
    }
    if (nom.length <6 && nom.length != 0){
        document.getElementById("indicMdp").innerText = "Votre mot de passe est au moins long de 6 caractères";
        checkMdp = false;
    }
    if (nom.length >= 6){
        document.getElementById("indicMdp").innerText = "";
        checkMdp = true;
    }
    allumage_Bouton();
}

function identification() {
nomSaisie = document.getElementById("nom").value;

users = JSON.parse(localStorage.getItem("utilisateursStorage"));

for (i=0;i<users.length;i++){
    if (users[i].name == nomSaisie){
        controle_mot_de_passe(users[i]);
        break;
    }
document.getElementById("indicUsername").innerText = "Ce nom d'utilisateur n'existe pas";
}
}

function controle_mot_de_passe(user){
    mdpSaisie = document.getElementById("mdp").value;
    if (user.pass == mdpSaisie){
        chargementDuProfil (user)
        return;
    }
document.getElementById("indicMdp").innerText = "Mot de passe incorrect";    
}

function chargementDuProfil (user){
    sessionStorage.setItem("user", user.name);
    sessionStorage.setItem("mail", user.mail);
    sessionStorage.setItem("historique", JSON.stringify(user.historique));
    sessionStorage.setItem("tableau", user.tableau);
    sessionStorage.setItem("taille", user.taille);
    window.location.href="profil.html";
}